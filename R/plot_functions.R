#' Make a boxplot visualizing numbers of attached cells that were counted in the
#' input table.
#'
#' @param count_tbl input table with cell counts by groups
#' @param axis_labels optional parameter that sets custom x axis labels
#' @param facet default TRUE, if FALSE plot will have no facets
#' @param fill_var variable to determine the fill aesthetics. either a factor
#'  column of the input table or a color.
#' @param fill_factor default TRUE. if FALSE box fill is not mapped to
#' aesthetics
#'
#' @return ggplot boxplot
#' @export
#' @importFrom magrittr "%>%"
#'
#' @examples
#'
plot_attCells_migPat_trt <-
  function(
    count_tbl,
    axis_labels = NULL,
    facet = TRUE,
    fill_var,
    fill_factor = TRUE
  ){
    fill_var <- dplyr::enquo(fill_var)
    ggplot2::ggplot(
      count_tbl,
      ggplot2::aes(
        x = treatment,
        y = n
      )
    ) +
    {if (fill_factor)
      ggplot2::geom_boxplot(
        ggplot2::aes_(
          fill = fill_var
        )
        # color = "black",
        # size = .5
      )
    } +
    {if (!fill_factor)
      ggplot2::geom_boxplot(
        color = "black",
        size = .5,
        fill = eval(rlang::UQE(fill_var))
      )
    } +
      theme_PhD() +
      ggplot2::theme(
        legend.position = "top",
        axis.text.x = ggplot2::element_text(angle = 90, vjust = 0.5),
        axis.line.x = ggplot2::element_blank(),
        #strip.text = ggplot2::element_text(size = 14, face = "bold"),
        axis.title.x = ggplot2::element_blank()
      ) +
      ggplot2::labs(fill = "") +
      ggplot2::guides(fill = ggplot2::guide_legend(nrow = 1)) +
      {if (!is.null(axis_labels))
        ggplot2::scale_x_discrete(labels = axis_labels)
      } +
      ggplot2::scale_fill_manual(values = sorted_palette) +
      {if (facet)
        ggplot2::facet_wrap(
          ~clusterid,
          nrow = 1,
          scales = "free"
        )
      }
  }

#' Make plot of the relative frequency of cell migratory patterns determined by
#' hierarchical clustering.
#'
#' @param data_tbl object of class data frame or tibble with observations to
#' count with columns for grouping
#' @param count_var character string or list of strings with variable by which
#' data_tbl should be grouped
#' @param pattern_palette palette used for fill aesthetics. Defaults to NULL which
#' results in using the NPG color palette from the ggsci package
#'
#' @return ggplot object
#' @export
#' @importFrom magrittr "%>%"
#'
#' @examples plot_migPat_distr_2_facet(mot_par, c("celltype", "clusterid"), celltype)
#'
plot_migPat_distr <-
  function(
    data_tbl,
    count_var,
    pattern_palette = NULL
  ){
    sorted_palette <-
      c("#deebf7", "#9ecae1", "#3182bd", "#fee0d2", "#fc9272", "#de2d26")
    freq_tbl <- data_tbl %>%
      dplyr::group_by(.dots = count_var) %>%
      dplyr::tally() %>%
      dplyr::mutate(rel.freq = n / sum(n))

    plot <- freq_tbl %>%
      ggplot2::ggplot(
        ggplot2::aes(
          x = celltype,
          y = rel.freq,
          fill = clusterid
        )
      ) +
      ggplot2::geom_col(position = "stack", width = .7) +
      ggplot2::theme_classic() +
      ggplot2::scale_y_continuous(labels = scales::percent) +
      {if (!is.null(pattern_palette))
        ggplot2::scale_fill_manual(values = pattern_palette)
      } +
      {if (is.null(pattern_palette))
        ggsci::scale_fill_npg()
      } +
      ggplot2::labs(
        y = "relative frequency",
        x = "",
        fill = "migratory pattern"
      ) +
      ggplot2::theme(
        axis.title.x = ggplot2::element_blank(),
        legend.position = "top"
      ) +
      ggplot2::guides(fill = ggplot2::guide_legend(nrow = 1, title.position = "top"))
    return(plot)
  }


#' Plot the percentage change of observed migratory patterns after a treatment
#'
#' @description This is a function to generate a column plot depicting the change
#' of observed migratory patterns in percentage reduction/increase after a treatment.
#'
#'
#' @param data a tbl/data frame to count, group and plot
#' @param pattern_palette supplies the palette for the fill aesthetics to ggplot.
#' defaults to NULL which results in using scale_fill_npg() from ggsci package.
#' @param facet_var variable for faceting by ggplot2. Defaults to NULL and can
#' only be "Tcell_line", "celltype" or NULL.
#' @param datapoints boolean. Controls if datapoints will plotted. Defaults to
#' FALSE
#'
#' @return ggplot depicting percentage change of migratory pattern after treatment
#' with celltype as facets.
#' @export
#'
#' @examples
plot_perc_change_per_cluster_trt <- function(
  data,
  pattern_palette = NULL,
  facet_var = NULL,
  datapoints = FALSE
) {
  if (is.null(facet_var)) {
    res <- data %>%
      dplyr::count(experimentID, movieID, treatment, clusterid) %>%
      dplyr::group_by(experimentID, treatment, clusterid) %>%
      dplyr::summarise(av_cells = mean(n)) %>%
      tidyr::complete(experimentID, treatment, clusterid, fill = list(av_cells = 0)) %>%
      dplyr::group_by(experimentID, clusterid) %>%
      dplyr::arrange(clusterid, .by_group = T) %>%
      dplyr::mutate(delta = ( dplyr::lead(av_cells) - av_cells ) / av_cells )

    plot <- ggplot2::ggplot(
      res,
      ggplot2::aes(
        x = clusterid,
        y = delta,
        fill = clusterid
      )
    ) +
      ggplot2::geom_bar(
        stat = "summary",
        fun.y = "mean"
      ) +
      { if (datapoints)
        ggplot2::geom_point(
          position = ggplot2::position_jitter(width = .1),
          alpha = .6,
          size = 2,
          show.legend = F
        )
      } +
      theme_PhD() +
      {if (!is.null(pattern_palette))
        ggplot2::scale_fill_manual(values = sorted_palette)
      } +
      {if (is.null(pattern_palette))
        ggsci::scale_fill_npg()
      } +
      ggplot2::scale_y_continuous(labels = scales::percent)

    return(plot)

  } else if ( facet_var == "leuko_sub") {
    facet_var_enq <- dplyr::enquo(facet_var)
    res <- data %>%
      dplyr::count(experimentID, movieID, leuko_sub, treatment, clusterid) %>%
      dplyr::group_by(experimentID, leuko_sub, treatment, clusterid) %>%
      dplyr::summarise(av_cells = mean(n)) %>%
      tidyr::complete(experimentID, leuko_sub, treatment, clusterid, fill = list(av_cells = 0)) %>%
      dplyr::group_by(experimentID, leuko_sub, clusterid) %>%
      dplyr::arrange(clusterid, .by_group = T) %>%
      dplyr::mutate(delta = ( dplyr::lead(av_cells) - av_cells ) / av_cells )

    plot <- ggplot2::ggplot(
      res,
      ggplot2::aes(
        x = clusterid,
        y = delta,
        fill = clusterid
      )
    ) +
      ggplot2::geom_bar(
        stat = "summary",
        fun.y = "mean"
      ) +
      { if (datapoints)
        ggplot2::geom_point(
          position = ggplot2::position_jitter(width = .1),
          alpha = .6,
          size = 2,
          show.legend = F
        )
      } +
      theme_PhD() +
      {if (!is.null(pattern_palette))
        ggplot2::scale_fill_manual(values = sorted_palette)
      } +
      {if (is.null(pattern_palette))
        ggsci::scale_fill_npg()
      } +
      ggplot2::scale_y_continuous(labels = scales::percent) +
      ggplot2::facet_grid(~ leuko_sub)
    return(plot)
  } else if ( facet_var == "Tcell_line") {
    facet_var_enq <- dplyr::enquo(facet_var)
    res <- data %>%
      dplyr::count(experimentID, movieID, Tcell_line, treatment, clusterid) %>%
      dplyr::group_by(experimentID, Tcell_line, treatment, clusterid) %>%
      dplyr::summarise(av_cells = mean(n)) %>%
      tidyr::complete(experimentID, Tcell_line, treatment, clusterid, fill = list(av_cells = 0)) %>%
      dplyr::group_by(experimentID, Tcell_line, clusterid) %>%
      dplyr::arrange(clusterid, .by_group = T) %>%
      dplyr::mutate(delta = ( dplyr::lead(av_cells) - av_cells ) / av_cells )

    plot <- ggplot2::ggplot(
      res,
      ggplot2::aes(
        x = clusterid,
        y = delta,
        fill = clusterid
      )
    ) +
      ggplot2::geom_bar(
        stat = "summary",
        fun.y = "mean"
      ) +
      { if (datapoints)
        ggplot2::geom_point(
          position = ggplot2::position_jitter(width = .1),
          alpha = .6,
          size = 2,
          show.legend = F
        )
      } +
      theme_PhD() +
      {if (!is.null(pattern_palette))
        ggplot2::scale_fill_manual(values = sorted_palette)
      } +
      {if (is.null(pattern_palette))
        ggsci::scale_fill_npg()
      } +
      ggplot2::scale_y_continuous(labels = scales::percent) +
      ggplot2::facet_grid(~ Tcell_line)
    return(plot)
  } else if ( facet_var == "celltype") {
    facet_var_enq <- dplyr::enquo(facet_var)
    res <- data %>%
      dplyr::count(experimentID, movieID, celltype, treatment, clusterid) %>%
      dplyr::group_by(experimentID, celltype, treatment, clusterid) %>%
      dplyr::summarise(av_cells = mean(n)) %>%
      tidyr::complete(experimentID, celltype, treatment, clusterid, fill = list(av_cells = 0)) %>%
      dplyr::group_by(experimentID, celltype, clusterid) %>%
      dplyr::arrange(clusterid, .by_group = T) %>%
      dplyr::mutate(delta = ( dplyr::lead(av_cells) - av_cells ) / av_cells )

    plot <- ggplot2::ggplot(
      res,
      ggplot2::aes(
        x = clusterid,
        y = delta,
        fill = clusterid
      )
    ) +
      ggplot2::geom_bar(
        stat = "summary",
        fun.y = "mean"
      ) +
      { if (datapoints)
        ggplot2::geom_point(
          position = ggplot2::position_jitter(width = .1),
          alpha = .6,
          size = 2,
          show.legend = F
        )
      } +
      theme_PhD() +
      {if (!is.null(pattern_palette))
        ggplot2::scale_fill_manual(values = sorted_palette)
      } +
      {if (is.null(pattern_palette))
        ggsci::scale_fill_npg()
      } +
      ggplot2::scale_y_continuous(labels = scales::percent) +
      ggplot2::facet_grid(~ celltype)
    return(plot)
  }

}


#' Plot the distribution of migratory patterns with 2 facets
#'
#' @param data_tbl object of class data frame or tibble with observations to
#' count with columns for grouping
#' @param count_var character string or list of strings with variable by which
#' data_tbl should be grouped
#' @param facet_var variable for facetting
#' @param pattern_palette palette used for fill aesthetics. Defaults to NULL which
#' results in using the NPG color palette from the ggsci package
#' @importFrom magrittr "%>%"
#' @return ggplot with 2 facets
#' @export
#'
#' @examples
plot_migPat_distr_2_facet <-
  function(
    data_tbl,
    count_var,
    facet_var = NULL,
    pattern_palette = NULL
  ){
    facet_var <- dplyr::enquo(facet_var)
    freq_tbl <- data_tbl %>%
      dplyr::group_by(.dots = count_var) %>%
      dplyr::tally() %>%
      dplyr::mutate(rel.freq = n / sum(n))

    freq_tbl %>%
      ggplot2::ggplot(
        ggplot2::aes(
          x = clusterid,
          y = rel.freq,
          fill = clusterid
        )
      ) +
      ggplot2::geom_col() +
      ggplot2::geom_text(
        ggplot2::aes(
          label = scales::percent(rel.freq),
          y = rel.freq
        ),
        vjust = - .5,
        size = 3
      ) +
      ggplot2::facet_grid(facet_var) +
      ggplot2::theme_classic() +
      ggplot2::scale_y_continuous(labels = scales::percent) +
      {if (!is.null(pattern_palette))
        ggplot2::scale_fill_manual(values = pattern_palette)
      } +
      {if (is.null(pattern_palette))
        ggsci::scale_fill_npg()
      } +
      ggplot2::labs(
        y = "relative frequency",
        x = "migratory pattern",
        fill = "migratory pattern"
      ) +
      ggplot2::theme(
        axis.title.x = ggplot2::element_blank(),
        axis.text.x = ggplot2::element_blank(),
        axis.ticks.x = ggplot2::element_blank(),
        axis.line.x = ggplot2::element_blank(),
        legend.position = "top",
        strip.placement = "outside"
      ) +
      ggplot2::guides(fill = ggplot2::guide_legend(nrow = 1))
  }

#' Plot percentage change
#' @description A function to plot observed cells after treatment in relation to
#' control situation in percentage change
#'
#' @param data tbl with variables experimentID, movieID, treatment, leuko_sub
#' @param fill_pal color palette for the fill aesthetic
#'
#' @return ggplot object
#' @export
#' @importFrom magrittr "%>%"
#'
#' @examples
plot_Dperc_to_ctrl <- function(
  data,
  fill_pal = NULL
  ) {
  data %>%
    count(
      experimentID,
      movieID,
      treatment,
      leuko_sub
    ) %>%
    group_by(
      experimentID,
      treatment,
      leuko_sub
    ) %>%
    summarise(av_cells = mean(n)) %>%
    group_by(experimentID, leuko_sub) %>%
    arrange(leuko_sub, .by_group = T) %>%
    mutate(delta = av_cells / lag(av_cells) ) %>%
    replace_na(list(delta = 1)) %>%
    filter(treatment != "control") %>%
    ggplot(
      aes(
        x = leuko_sub,
        y = delta,
        fill = leuko_sub
      )
    ) +
    geom_bar(
      stat = "summary",
      fun.y = "mean",
      position = position_dodge(width = .90),
      width = .7
    ) +
    geom_point(
      position = position_jitterdodge(jitter.width = .1, dodge.width = .8),
      alpha = 0.6,
      show.legend = F
    ) +
    theme_PhD() +
    ylim(0,1) +
    { if (!is.null(fill_pal))
    scale_fill_manual(values = fill_pal)
    } +
    { if (is.null(fill_pal))
      ggsci::scale_fill_npg()
    } +
    scale_y_continuous(labels = scales::percent) +
    ylab("# of cells [%]") +
    theme(axis.title.x = element_blank())
}
